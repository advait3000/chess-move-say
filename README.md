# Chess Move Say

A Chrome extension that reads aloud chess moves when playing on online chess sites.

## Supported Sites
- [x] [Lichess](https://lichess.org)
- [ ] [chess.com](https://www.chess.com)

## Roadmap
- [ ] Remove background page and panel page cruft from boilerplate
- [ ] Clicking on the icon should enable/disable voice
- [ ] Add custom icons
- [ ] Add webpack-crx
- [ ] Publish to Chrome Web Store
- [ ] Add support for multiple voices

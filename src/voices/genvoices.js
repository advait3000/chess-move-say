// Model configuration
const languageCode = "en-GB";
const modelName = "en-GB-Wavenet-A";
const speakingRate = 1.34;

// FFMPEG configuration
const ffmpegDBThreshold = "-50dB";
const ffmpegDetectionType = "rms";
const ffmpegDetectionWindow = "0.01";

const pieces = ["rook", "knight", "bishop", "queen", "king"];
const files = ["A", "B", "C", "D", "E", "F", "G", "H"];
const ranks = ["1", "2", "3", "4", "5", "6", "7", "8"];
const squares = files.flatMap((file) => {
  return ranks.map(rank => file + " " + rank);
});
const verbs = ["takes", "check", "checkmate", "castles short", "castles long", "promotes to"];
const allPhrases = [pieces, files, ranks, squares, verbs].flat();
console.log(allPhrases);

// Imports the Google Cloud client library
const textToSpeech = require("@google-cloud/text-to-speech");

// Import other required libraries
const fs = require("fs");
const util = require("util");
const writeFile = util.promisify(fs.writeFile);
const child_process = require("child_process");
const exec = util.promisify(child_process.exec);

// Creates a client
const client = new textToSpeech.TextToSpeechClient();


async function _generateSound(text, filename, startPeriods = 1, stopPeriods = -1) {
  if (fs.existsSync(filename)) {
    console.log("File already exits, skipping: " + filename);
    return;
  }

  // Construct the request
  const request = {
    input: { ssml: "<ssml>" + text + "</ssml>" },
    // Select the language and SSML Voice Gender (optional)
    voice: {
      languageCode: languageCode,
      name: modelName,
    },
    // Select the type of audio encoding
    audioConfig: {
      audioEncoding: "LINEAR16",
      speakingRate: speakingRate,
    },
  };

  // Performs the Text-to-Speech request
  const [response] = await client.synthesizeSpeech(request);
  // Write the binary audio content to a local file
  await writeFile(filename, response.audioContent, "binary");
  console.log("Audio content written to file: " + filename);

  // Trim sound
  const outFilename = filename.replace(".wav", ".short2.wav");
  const cmd = `ffmpeg -y -i "${filename}" -af silenceremove=start_periods=${startPeriods}:start_threshold=${ffmpegDBThreshold}:stop_periods=${stopPeriods}:stop_threshold=${ffmpegDBThreshold}:detection=${ffmpegDetectionType}:window=${ffmpegDetectionWindow} "${outFilename}"`;
  console.log(cmd);
  await exec(cmd);
}

async function generatePieceSound(text) {
  const filename = (text + ".wav").replace(/['"<>\\/]/g, "_");
  if (pieces.includes(text)) {
    text = text + "<break time='100ms' /> d 4";
    await _generateSound(text, filename, 1, 1);
  } else if (text == "takes") {
    text = "Queen <break time=\"100ms\"/> takes <break time=\"100ms\"/> d 4";
    await _generateSound(text, filename, 2, 2);
  } else {
    await _generateSound(text, filename, 1, -1);
  }
}

async function main() {
  for (const s of allPhrases) {
    await generatePieceSound(s);
  }
}

main();

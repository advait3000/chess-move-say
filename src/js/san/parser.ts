import { fileRankToSquareToken, stringToToken, Token, tokenToString } from "./tokens";

/**
 * Function that returns an array of possible Matches based on the input.
 * Note that there can be multiple matches in the case of Optional, etc.
 */
type Matcher = (Pos) => IMatch[];

/**
 * Represents a specific position in an input string denoted by the offset.
 */
interface IPos {
  in: string;
  offset: number;
}

/**
 * Converts the Pos into its remaining string (dropping the offset).
 */
function toString(p: IPos): string {
  return p.in.substring(p.offset);
}

/**
 * Represents a potential match on the input Pos of length len.
 */
interface IMatch {
  pos: IPos;
  len: number;
  children: IMatch[];
  token: Token | null;
}

/**
 * Wraps the given Match with the given Token.
 */
function wrapWithToken(t: Token): (Match) => IMatch {
  return (m) => {
    return {
      ...m,
      children: [m],
      token: t,
    };
  };
}

/**
 * Returns the next Pos based on the length of this match.
 * @param m
 */
function nextPos(m: IMatch): IPos {
  return {
    in: m.pos.in,
    offset: m.pos.offset + m.len,
  };
}

/**
 * Parse the SAN returning an Array interpretation
 */
export function matchSan(s: string): Token[] | null {
  const rec = (m: IMatch): Token[] => {
    const mySay = m.token == null ? [] : [m.token];
    return m.children.flatMap(rec).concat(mySay);
  };

  const matches = matchString(sanMatcher, s).map(rec);
  if (matches.length > 1) {
    console.error("Failed to unambiguously parse SAN: " + s);
    return matches[0];
  } else if (matches.length === 0) {
    console.error("Failed to parse SAN: ", + s);
    return null;
  }
  return matches[0];
}

/**
 * Returns a set of matches that *fully consume* the input string.
 */
export function matchString(m: Matcher, s: string): IMatch[] {
  const pos = {
    in: s,
    offset: 0,
  };
  return m(pos).filter((match) => {
    // Filter matches that don't entirely consume input
    return toString(nextPos(match)) === "";
  });
}

/**
 * Matches a hardcoded string S.
 */
export function stringMatcher(s: string): Matcher {
  return (pos) => {
    if (toString(pos).startsWith(s)) {
      return [{
        children: [],
        len: s.length,
        pos,
        token: null,
      }];
    } else {
      return [];
    }
  };
}

/**
 * Matches both the first and second matcher together.
 */
export function consMatcher(car: Matcher, cdr: Matcher): Matcher {
  return (pos) => {
    return car(pos).flatMap((carMatch) => {
      return cdr(nextPos(carMatch))
        .map((cdrMatch) => {
          return {
            children: [carMatch, cdrMatch],
            len: carMatch.len + cdrMatch.len,
            pos,
            token: null,
          };
        });
    });
  };
}

/**
 * Wraps an existing matcher, marking it as optional.
 */
export function optionalMatcher(m: Matcher) {
  return (pos) => {
    // First try to greedily match
    return m(pos).map(wrapWithToken(null)).concat([{
      // Always include omitted match
      children: [],
      len: 0,
      pos,
      token: null,
    }]);
  };
}

/**
 * Matches the provided Matchers in sequence.
 */
export function arrayMatcher(matchers: Matcher[]): Matcher {
  const rec = (ms: Matcher[]) => {
    if (ms.length < 2) {
      throw new Error("ArrayMatcher must be used with two or more elements");
    } else if (ms.length === 2) {
      return consMatcher(ms[0], ms[1]);
    } else {
      return consMatcher(ms[0], rec(ms.slice(1)));
    }
  };
  const m = rec(matchers);
  return (pos) => {
    return m(pos).map(wrapWithToken(null));
  };
}

/**
 * Matches any of the provided Matchers.
 */
export function orMatcher(ms: Matcher[]): Matcher {
  return (pos) => {
    return ms.flatMap((m) => m(pos))
      .map(wrapWithToken(null));
  };
}

/**
 * Matches the string for a given token, adding token metadata to the Match.
 * @param t
 */
function tokenMatcher(t: Token): Matcher {
  return (pos: IPos) => {
    return stringMatcher(tokenToString(t))(pos)
      .map(wrapWithToken(t));
  };
}

/**
 * Matches any non-pawn piece.
 */
const pieceMatcher: Matcher = orMatcher([
  tokenMatcher(Token.Rook),
  tokenMatcher(Token.Knight),
  tokenMatcher(Token.Bishop),
  tokenMatcher(Token.Queen),
  tokenMatcher(Token.King),
]);

/**
 * Matches a chess board file (column).
 */
const fileMatcher: Matcher = orMatcher([
  tokenMatcher(Token.FileA),
  tokenMatcher(Token.FileB),
  tokenMatcher(Token.FileC),
  tokenMatcher(Token.FileD),
  tokenMatcher(Token.FileE),
  tokenMatcher(Token.FileF),
  tokenMatcher(Token.FileG),
  tokenMatcher(Token.FileH),
]);

/**
 * Matches a chess board rank (row).
 */
const rankMatcher: Matcher = orMatcher([
  tokenMatcher(Token.Rank1),
  tokenMatcher(Token.Rank2),
  tokenMatcher(Token.Rank3),
  tokenMatcher(Token.Rank4),
  tokenMatcher(Token.Rank5),
  tokenMatcher(Token.Rank6),
  tokenMatcher(Token.Rank7),
  tokenMatcher(Token.Rank8),
]);

/**
 * Matches the "x" denoting a capture.
 */
const capturesMatcher: Matcher = tokenMatcher(Token.Captures);

/**
 * Matches a chess board square.
 */
const squareMatcher: Matcher = (p: IPos): IMatch[] => {
  return consMatcher(fileMatcher, rankMatcher)(p).map((m) => {
    const san = toString(m.pos).substring(0, m.len);
    return {
      ...m,
      children: [],
      token: stringToToken(san),
    };
  });
};

/**
 * Matches either long or short castling.
 */
const castlesMatcher = orMatcher([
  tokenMatcher(Token.CastlesLong),
  tokenMatcher(Token.CastlesShort),
]);

/**
 * Matches the "=" denoting pawn promotion.
 */
const promotesToMatcher: Matcher = tokenMatcher(Token.PromotesTo);

/**
 * Matches the whole promotion clause including the promotion piece.
 */
const promotionMatcher = consMatcher(promotesToMatcher, pieceMatcher);

/**
 * Matches check or checkmate.
 */
const checkOrMateMatcher = orMatcher([
  tokenMatcher(Token.Check),
  tokenMatcher(Token.Checkmate),
]);

/**
 * Matches a non-castling move.
 */
const regularMoveMatcher = arrayMatcher([
  optionalMatcher(pieceMatcher),
  optionalMatcher(fileMatcher),
  optionalMatcher(rankMatcher),
  optionalMatcher(capturesMatcher),
  squareMatcher,
  optionalMatcher(promotionMatcher),
]);

/**
 * Matches any chess SAN move.
 */
const sanMatcher = arrayMatcher([
  orMatcher([regularMoveMatcher, castlesMatcher]),
  optionalMatcher(checkOrMateMatcher),
]);

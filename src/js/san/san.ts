import { matchSan } from "./parser";
import { sayToken } from "./tokens";

/**
 * Parse SAN into a voicable string.
 */
export function parseSan(san: string): string {
  const matches = matchSan(san).map(sayToken);
  if (matches.length === 0) {
    console.error("Failed to parse san", san);
    return "";
  }
  return matches.join(" ");
}

import { parseSan } from "./san";

test("parseSan", () => {
  const verify = (san: string, expected: string) => {
    expect(parseSan(san)).toBe(expected);
  };

  verify("f4", "F 4");
  verify("Nf4", "knight F 4");
  verify("Qf4", "queen F 4");
  verify("Qf4+", "queen F 4 check");
  verify("O-O-O#", "castles long checkmate");
});

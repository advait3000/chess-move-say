/**
 * Represents all the possible tokens of SAN.
 */
export enum Token {
  Rook,
  Bishop,
  Knight,
  Queen,
  King,
  FileA,
  FileB,
  FileC,
  FileD,
  FileE,
  FileF,
  FileG,
  FileH,
  Rank1,
  Rank2,
  Rank3,
  Rank4,
  Rank5,
  Rank6,
  Rank7,
  Rank8,
  A1,
  A2,
  A3,
  A4,
  A5,
  A6,
  A7,
  A8,
  B1,
  B2,
  B3,
  B4,
  B5,
  B6,
  B7,
  B8,
  C1,
  C2,
  C3,
  C4,
  C5,
  C6,
  C7,
  C8,
  D1,
  D2,
  D3,
  D4,
  D5,
  D6,
  D7,
  D8,
  E1,
  E2,
  E3,
  E4,
  E5,
  E6,
  E7,
  E8,
  F1,
  F2,
  F3,
  F4,
  F5,
  F6,
  F7,
  F8,
  G1,
  G2,
  G3,
  G4,
  G5,
  G6,
  G7,
  G8,
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  H7,
  H8,
  Captures,
  CastlesLong,
  CastlesShort,
  Check,
  Checkmate,
  PromotesTo,
}

const tokenEncodings = {
  "R": Token.Rook,
  "B": Token.Bishop,
  "N": Token.Knight,
  "Q": Token.Queen,
  "K": Token.King,
  "a": Token.FileA,
  "b": Token.FileB,
  "c": Token.FileC,
  "d": Token.FileD,
  "e": Token.FileE,
  "f": Token.FileF,
  "g": Token.FileG,
  "h": Token.FileH,
  "1": Token.Rank1,
  "2": Token.Rank2,
  "3": Token.Rank3,
  "4": Token.Rank4,
  "5": Token.Rank5,
  "6": Token.Rank6,
  "7": Token.Rank7,
  "8": Token.Rank8,
  "a1": Token.A1,
  "a2": Token.A2,
  "a3": Token.A3,
  "a4": Token.A4,
  "a5": Token.A5,
  "a6": Token.A6,
  "a7": Token.A7,
  "a8": Token.A8,
  "b1": Token.B1,
  "b2": Token.B2,
  "b3": Token.B3,
  "b4": Token.B4,
  "b5": Token.B5,
  "b6": Token.B6,
  "b7": Token.B7,
  "b8": Token.B8,
  "c1": Token.C1,
  "c2": Token.C2,
  "c3": Token.C3,
  "c4": Token.C4,
  "c5": Token.C5,
  "c6": Token.C6,
  "c7": Token.C7,
  "c8": Token.C8,
  "d1": Token.D1,
  "d2": Token.D2,
  "d3": Token.D3,
  "d4": Token.D4,
  "d5": Token.D5,
  "d6": Token.D6,
  "d7": Token.D7,
  "d8": Token.D8,
  "e1": Token.E1,
  "e2": Token.E2,
  "e3": Token.E3,
  "e4": Token.E4,
  "e5": Token.E5,
  "e6": Token.E6,
  "e7": Token.E7,
  "e8": Token.E8,
  "f1": Token.F1,
  "f2": Token.F2,
  "f3": Token.F3,
  "f4": Token.F4,
  "f5": Token.F5,
  "f6": Token.F6,
  "f7": Token.F7,
  "f8": Token.F8,
  "g1": Token.G1,
  "g2": Token.G2,
  "g3": Token.G3,
  "g4": Token.G4,
  "g5": Token.G5,
  "g6": Token.G6,
  "g7": Token.G7,
  "g8": Token.G8,
  "h1": Token.H1,
  "h2": Token.H2,
  "h3": Token.H3,
  "h4": Token.H4,
  "h5": Token.H5,
  "h6": Token.H6,
  "h7": Token.H7,
  "h8": Token.H8,
  "x": Token.Captures,
  "O-O-O": Token.CastlesLong,
  "O-O": Token.CastlesShort,
  "+": Token.Check,
  "#": Token.Checkmate,
  "=": Token.PromotesTo,
};

/**
 * Given a SAN string, return the corresponding token if it exists.
 */
export function stringToToken(s: string): Token | undefined {
  return tokenEncodings[s];
}

/**
 * Converts the given file and rank tokens into a single square token.
 */
export function fileRankToSquareToken(fileAndRank: Token[]): Token | undefined {
  const san = fileAndRank.map(tokenToString).join("");
  return stringToToken(san);
}

/**
 * Given a Token, return the corresponding SAN string.
 */
export function tokenToString(t1: Token): string {
  for (const [s, t2] of Object.entries(tokenEncodings)) {
    if (t1 === t2) {
      return s;
    }
  }
  throw new Error("Could not find Token: " + t1);
}

const tokenPronounciation = {};
tokenPronounciation[Token.Rook] = "rook";
tokenPronounciation[Token.Bishop] = "bioshop";
tokenPronounciation[Token.Knight] = "knight";
tokenPronounciation[Token.Queen] = "queen";
tokenPronounciation[Token.King] = "king";
tokenPronounciation[Token.FileA] = "A";
tokenPronounciation[Token.FileB] = "B";
tokenPronounciation[Token.FileC] = "C";
tokenPronounciation[Token.FileD] = "D";
tokenPronounciation[Token.FileE] = "E";
tokenPronounciation[Token.FileF] = "F";
tokenPronounciation[Token.FileG] = "G";
tokenPronounciation[Token.FileH] = "H";
tokenPronounciation[Token.Rank1] = "1";
tokenPronounciation[Token.Rank2] = "2";
tokenPronounciation[Token.Rank3] = "3";
tokenPronounciation[Token.Rank4] = "4";
tokenPronounciation[Token.Rank5] = "5";
tokenPronounciation[Token.Rank6] = "6";
tokenPronounciation[Token.Rank7] = "7";
tokenPronounciation[Token.Rank8] = "8";
tokenPronounciation[Token.A1] = "A 1";
tokenPronounciation[Token.A2] = "A 2";
tokenPronounciation[Token.A3] = "A 3";
tokenPronounciation[Token.A4] = "A 4";
tokenPronounciation[Token.A5] = "A 5";
tokenPronounciation[Token.A6] = "A 6";
tokenPronounciation[Token.A7] = "A 7";
tokenPronounciation[Token.A8] = "A 8";
tokenPronounciation[Token.B1] = "B 1";
tokenPronounciation[Token.B2] = "B 2";
tokenPronounciation[Token.B3] = "B 3";
tokenPronounciation[Token.B4] = "B 4";
tokenPronounciation[Token.B5] = "B 5";
tokenPronounciation[Token.B6] = "B 6";
tokenPronounciation[Token.B7] = "B 7";
tokenPronounciation[Token.B8] = "B 8";
tokenPronounciation[Token.C1] = "C 1";
tokenPronounciation[Token.C2] = "C 2";
tokenPronounciation[Token.C3] = "C 3";
tokenPronounciation[Token.C4] = "C 4";
tokenPronounciation[Token.C5] = "C 5";
tokenPronounciation[Token.C6] = "C 6";
tokenPronounciation[Token.C7] = "C 7";
tokenPronounciation[Token.C8] = "C 8";
tokenPronounciation[Token.D1] = "D 1";
tokenPronounciation[Token.D2] = "D 2";
tokenPronounciation[Token.D3] = "D 3";
tokenPronounciation[Token.D4] = "D 4";
tokenPronounciation[Token.D5] = "D 5";
tokenPronounciation[Token.D6] = "D 6";
tokenPronounciation[Token.D7] = "D 7";
tokenPronounciation[Token.D8] = "D 8";
tokenPronounciation[Token.E1] = "E 1";
tokenPronounciation[Token.E2] = "E 2";
tokenPronounciation[Token.E3] = "E 3";
tokenPronounciation[Token.E4] = "E 4";
tokenPronounciation[Token.E5] = "E 5";
tokenPronounciation[Token.E6] = "E 6";
tokenPronounciation[Token.E7] = "E 7";
tokenPronounciation[Token.E8] = "E 8";
tokenPronounciation[Token.F1] = "F 1";
tokenPronounciation[Token.F2] = "F 2";
tokenPronounciation[Token.F3] = "F 3";
tokenPronounciation[Token.F4] = "F 4";
tokenPronounciation[Token.F5] = "F 5";
tokenPronounciation[Token.F6] = "F 6";
tokenPronounciation[Token.F7] = "F 7";
tokenPronounciation[Token.F8] = "F 8";
tokenPronounciation[Token.G1] = "G 1";
tokenPronounciation[Token.G2] = "G 2";
tokenPronounciation[Token.G3] = "G 3";
tokenPronounciation[Token.G4] = "G 4";
tokenPronounciation[Token.G5] = "G 5";
tokenPronounciation[Token.G6] = "G 6";
tokenPronounciation[Token.G7] = "G 7";
tokenPronounciation[Token.G8] = "G 8";
tokenPronounciation[Token.H1] = "H 1";
tokenPronounciation[Token.H2] = "H 2";
tokenPronounciation[Token.H3] = "H 3";
tokenPronounciation[Token.H4] = "H 4";
tokenPronounciation[Token.H5] = "H 5";
tokenPronounciation[Token.H6] = "H 6";
tokenPronounciation[Token.H7] = "H 7";
tokenPronounciation[Token.H8] = "H 8";
tokenPronounciation[Token.Captures] = "takes";
tokenPronounciation[Token.CastlesLong] = "castles long";
tokenPronounciation[Token.CastlesShort] = "castles short";
tokenPronounciation[Token.Check] = "check";
tokenPronounciation[Token.Checkmate] = "checkmate";
tokenPronounciation[Token.PromotesTo] = "promotes to";

/**
 * Return the english pronounciation of the given Token.
 */
export function sayToken(t: Token): string {
  return tokenPronounciation[t];
}

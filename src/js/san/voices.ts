import { matchSan } from "./parser";
import { Token } from "./tokens";

/**
 * Keep track of globally playing sound so no sounds overlap.
 */
let currentSound = Promise.resolve();

export function say(san: string) {
  const tokens = matchSan(san);
  if (!tokens) {
    console.warn("Failed to parse SAN: " + san);
    return;
  }

  return currentSound = currentSound.then(() => {
    return playAll(tokens.map(getFileName).map(getPlayer));
  });
}

const fileNames = {};
fileNames[Token.Rook] = "rook.short2.wav";
fileNames[Token.Bishop] = "bishop.short2.wav";
fileNames[Token.Knight] = "knight.short2.wav";
fileNames[Token.Queen] = "queen.short2.wav";
fileNames[Token.King] = "king.short2.wav";
fileNames[Token.FileA] = "A.short2.wav";
fileNames[Token.FileB] = "B.short2.wav";
fileNames[Token.FileC] = "C.short2.wav";
fileNames[Token.FileD] = "D.short2.wav";
fileNames[Token.FileE] = "E.short2.wav";
fileNames[Token.FileF] = "F.short2.wav";
fileNames[Token.FileG] = "G.short2.wav";
fileNames[Token.FileH] = "H.short2.wav";
fileNames[Token.Rank1] = "1.short2.wav";
fileNames[Token.Rank2] = "2.short2.wav";
fileNames[Token.Rank3] = "3.short2.wav";
fileNames[Token.Rank4] = "4.short2.wav";
fileNames[Token.Rank5] = "5.short2.wav";
fileNames[Token.Rank6] = "6.short2.wav";
fileNames[Token.Rank7] = "7.short2.wav";
fileNames[Token.Rank8] = "8.short2.wav";
fileNames[Token.A1] = "A 1.short2.wav";
fileNames[Token.A2] = "A 2.short2.wav";
fileNames[Token.A3] = "A 3.short2.wav";
fileNames[Token.A4] = "A 4.short2.wav";
fileNames[Token.A5] = "A 5.short2.wav";
fileNames[Token.A6] = "A 6.short2.wav";
fileNames[Token.A7] = "A 7.short2.wav";
fileNames[Token.A8] = "A 8.short2.wav";
fileNames[Token.B1] = "B 1.short2.wav";
fileNames[Token.B2] = "B 2.short2.wav";
fileNames[Token.B3] = "B 3.short2.wav";
fileNames[Token.B4] = "B 4.short2.wav";
fileNames[Token.B5] = "B 5.short2.wav";
fileNames[Token.B6] = "B 6.short2.wav";
fileNames[Token.B7] = "B 7.short2.wav";
fileNames[Token.B8] = "B 8.short2.wav";
fileNames[Token.C1] = "C 1.short2.wav";
fileNames[Token.C2] = "C 2.short2.wav";
fileNames[Token.C3] = "C 3.short2.wav";
fileNames[Token.C4] = "C 4.short2.wav";
fileNames[Token.C5] = "C 5.short2.wav";
fileNames[Token.C6] = "C 6.short2.wav";
fileNames[Token.C7] = "C 7.short2.wav";
fileNames[Token.C8] = "C 8.short2.wav";
fileNames[Token.D1] = "D 1.short2.wav";
fileNames[Token.D2] = "D 2.short2.wav";
fileNames[Token.D3] = "D 3.short2.wav";
fileNames[Token.D4] = "D 4.short2.wav";
fileNames[Token.D5] = "D 5.short2.wav";
fileNames[Token.D6] = "D 6.short2.wav";
fileNames[Token.D7] = "D 7.short2.wav";
fileNames[Token.D8] = "D 8.short2.wav";
fileNames[Token.E1] = "E 1.short2.wav";
fileNames[Token.E2] = "E 2.short2.wav";
fileNames[Token.E3] = "E 3.short2.wav";
fileNames[Token.E4] = "E 4.short2.wav";
fileNames[Token.E5] = "E 5.short2.wav";
fileNames[Token.E6] = "E 6.short2.wav";
fileNames[Token.E7] = "E 7.short2.wav";
fileNames[Token.E8] = "E 8.short2.wav";
fileNames[Token.F1] = "F 1.short2.wav";
fileNames[Token.F2] = "F 2.short2.wav";
fileNames[Token.F3] = "F 3.short2.wav";
fileNames[Token.F4] = "F 4.short2.wav";
fileNames[Token.F5] = "F 5.short2.wav";
fileNames[Token.F6] = "F 6.short2.wav";
fileNames[Token.F7] = "F 7.short2.wav";
fileNames[Token.F8] = "F 8.short2.wav";
fileNames[Token.G1] = "G 1.short2.wav";
fileNames[Token.G2] = "G 2.short2.wav";
fileNames[Token.G3] = "G 3.short2.wav";
fileNames[Token.G4] = "G 4.short2.wav";
fileNames[Token.G5] = "G 5.short2.wav";
fileNames[Token.G6] = "G 6.short2.wav";
fileNames[Token.G7] = "G 7.short2.wav";
fileNames[Token.G8] = "G 8.short2.wav";
fileNames[Token.H1] = "H 1.short2.wav";
fileNames[Token.H2] = "H 2.short2.wav";
fileNames[Token.H3] = "H 3.short2.wav";
fileNames[Token.H4] = "H 4.short2.wav";
fileNames[Token.H5] = "H 5.short2.wav";
fileNames[Token.H6] = "H 6.short2.wav";
fileNames[Token.H7] = "H 7.short2.wav";
fileNames[Token.H8] = "H 8.short2.wav";
fileNames[Token.Captures] = "takes.short2.wav";
fileNames[Token.CastlesLong] = "castles long.short2.wav";
fileNames[Token.CastlesShort] = "castles short.short2.wav";
fileNames[Token.Check] = "check.short2.wav";
fileNames[Token.Checkmate] = "checkmate.short2.wav";
fileNames[Token.PromotesTo] = "promotes to.short2.wav";

function getFileName(t: Token): string {
  // return "en-GB-Wavenet-A/" + fileNames[t];
  return fileNames[t];
}

interface IPlayable {
  play: () => Promise<any>;
}

function getPlayer(file: string): IPlayable {
  return {
    play: () => {
      return new Promise((resolve, reject) => {
        console.log("Playing ", file);
        const audio = new Audio(chrome.runtime.getURL(file));
        audio.addEventListener("ended", resolve);
        audio.addEventListener("error", reject);
        audio.play();
      });
    },
  };
}

function playAll(items: IPlayable[]): Promise<any> {
  let p = Promise.resolve();
  for (const item of items) {
    p = p.then(() => {
      return item.play();
    });
  }
  return p;
}

import { arrayMatcher, consMatcher, matchSan, matchString, optionalMatcher, orMatcher, stringMatcher } from "./parser";
import { Token } from "./tokens";

test("StringMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = stringMatcher("he");
  expect(matchString(m1, "h").length).toBe(1);
  expect(matchString(m1, "h2").length).toBe(0);
  expect(matchString(m1, "hh").length).toBe(0);
  expect(matchString(m2, "h").length).toBe(0);
  expect(matchString(m2, "hh").length).toBe(0);
  expect(matchString(m2, "he").length).toBe(1);
});

test("ConsMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = stringMatcher("e");
  const m3 = consMatcher(m1, m2);

  const matches = matchString(m3, "he");
  expect(matches.length).toBe(1);
  expect(matches[0].pos.offset).toBe(0);
  expect(matches[0].len).toBe(2);
  expect(matches[0].children.length).toBe(2);

  expect(matchString(m3, "hee").length).toBe(0);
  expect(matchString(m3, "be").length).toBe(0);
});

test("OptionalMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = optionalMatcher(m1);

  let matches = matchString(m2, "h");
  expect(matches.length).toBe(1);
  expect(matches[0].len).toBe(1);
  expect(matches[0].children.length).toBe(1);
  expect(matches[0].children[0].len).toBe(1);

  const m3 = stringMatcher("e");
  const m4 = consMatcher(m2, m3);

  matches = matchString(m4, "he");
  expect(matches.length).toBe(1);
  expect(matches[0].len).toBe(2);
  expect(matches[0].children.length).toBe(2);
  expect(matches[0].children[0].len).toBe(1);
  expect(matches[0].children[1].len).toBe(1);

  matches = matchString(m4, "e");
  expect(matches.length).toBe(1);
  expect(matches[0].len).toBe(1);
  expect(matches[0].children.length).toBe(2);
  expect(matches[0].children[0].len).toBe(0);
  expect(matches[0].children[1].len).toBe(1);

  const m5 = consMatcher(m2, m2);
  expect(matchString(m5, "").length).toBe(1);
  expect(matchString(m5, "h").length).toBe(2);
  expect(matchString(m5, "hh").length).toBe(1);
  expect(matchString(m5, "he").length).toBe(0);
  expect(matchString(m5, "e").length).toBe(0);
});

test("Chained ConsMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = consMatcher(m1, consMatcher(m1, m1));

  const matches = matchString(m2, "hhh");
  expect(matches.length).toBe(1);
  expect(matches[0].len).toBe(3);

  const m3 = optionalMatcher(m1);
  const m4 = stringMatcher("e");
  const m5 = consMatcher(m1, consMatcher(m4, m3));

  expect(matchString(m5, "heh").length).toBe(1);
  expect(matchString(m5, "he").length).toBe(1);
  expect(matchString(m5, "hee").length).toBe(0);
  expect(matchString(m5, "h").length).toBe(0);
});

test("ArrayMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = optionalMatcher(m1);
  const m3 = stringMatcher("e");
  const m4 = arrayMatcher([m1, m2, m3]);

  const matches = matchString(m4, "h");
  expect(matchString(m4, "he").length).toBe(1);
  expect(matchString(m4, "hhe").length).toBe(1);
});

test("OrMatcher", () => {
  const m1 = stringMatcher("h");
  const m2 = stringMatcher("e");
  const m3 = orMatcher([m1, m2]);

  expect(matchString(m3, "h").length).toBe(1);
  expect(matchString(m3, "e").length).toBe(1);
  expect(matchString(m3, "he").length).toBe(0);
  expect(matchString(m3, "1").length).toBe(0);
});

test("SanMatcher", () => {
  expect(matchSan("e4")).toStrictEqual([Token.E4]);
  expect(matchSan("e4+")).toStrictEqual([Token.E4, Token.Check]);
  expect(matchSan("Ne4")).toStrictEqual([Token.Knight, Token.E4]);
  expect(matchSan("O-O")).toStrictEqual([Token.CastlesShort]);
  expect(matchSan("O-O-O")).toStrictEqual([Token.CastlesLong]);
  expect(matchSan("O-O-O+")).toStrictEqual([Token.CastlesLong, Token.Check]);
  expect(matchSan("e8=Q")).toStrictEqual([Token.E8, Token.PromotesTo, Token.Queen]);
  expect(matchSan("Nbc4")).toStrictEqual([Token.Knight, Token.FileB, Token.C4]);
  expect(matchSan("Nbxh6")).toStrictEqual([Token.Knight, Token.FileB, Token.Captures, Token.H6]);
  expect(matchSan("Nbxa4#")).toStrictEqual([Token.Knight, Token.FileB, Token.Captures, Token.A4, Token.Checkmate]);
});

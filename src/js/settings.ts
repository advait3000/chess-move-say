const STORAGE_KEY = "enabled";

async function getKey<T>(k: string, defaultValue: T): Promise<T> {
  return await new Promise((resolve) => {
    chrome.storage.local.get([k], (items) => {
      try {
        resolve(JSON.parse(items[k]));
      } catch {
        resolve(defaultValue);
      }
    });
  });
}

async function setKey<T>(k: string, v: T): Promise<void> {
  return await new Promise((resolve) => {
    const obj = {};
    obj[k] = JSON.stringify(v);
    chrome.storage.local.set(obj, resolve);
  });
}

/**
 * Returns whether the extension should be enabled.
 */
export async function isEnabled(): Promise<boolean> {
  return (await getKey(STORAGE_KEY, true));
}

/**
 * Sets whether the extension should be enabled.
 */
export async function setEnabled(enabled: boolean): Promise<void> {
  await setKey(STORAGE_KEY, enabled);
}

/**
 * Toggles the enabled setting, returning the new value.
 */
export async function toggleEnabled(): Promise<boolean> {
  const enabled = await isEnabled();
  await setEnabled(!enabled);
  return !enabled;
}

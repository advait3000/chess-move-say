import { toggleEnabled as togggleEnabledSetting } from "./settings";

chrome.runtime.onInstalled.addListener(() => {
  chrome.browserAction.onClicked.addListener(toggleEnabled);
});

async function toggleEnabled() {
  const enabled = await togggleEnabledSetting();
  const icon = (enabled ? blueIcons : grayIcons);
  const title = (enabled ? "Chess Move Say (Enabled)" : "Chess Move Say (Disabled)");
  chrome.browserAction.setIcon({ path: icon });
  chrome.browserAction.setTitle({ title });
}

const blueIcons = {
  32: "/icon-32.png",
  64: "/icon-64.png",
  128: "/icon-128.png",
  256: "/icon-256.png",
  512: "/icon-512.png",
};

const grayIcons = {
  32: "/icon-gray-32.png",
  64: "/icon-gray-64.png",
  128: "/icon-gray-128.png",
  256: "/icon-gray-256.png",
  512: "/icon-gray-512.png",
};

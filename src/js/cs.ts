import { CUSTOM_EVENT_NAME } from "./const";
import { say } from "./san";
import { isEnabled } from "./settings";

const s = document.createElement("script");
s.src = chrome.runtime.getURL("direct_script.bundle.js");
(document.head || document.documentElement).appendChild(s);
s.onload = () => {
  s.remove();
};

export type LichessStreamEvent = CustomEvent<{ name: string, body: any }>;

// Events from lichess body
document.addEventListener(CUSTOM_EVENT_NAME, (e: LichessStreamEvent) => {
  if (e.detail.name !== "socket.in.move") {
    return;
  }

  if (!e.detail.body || !e.detail.body.san) {
    console.log("Malformed event", e);
    return;
  }

  console.log("Saying:", e.detail.body.san);
  handleSan(e.detail.body.san);
});

async function handleSan(san: string) {
  const enabled = await isEnabled();
  if (enabled) {
    say(san);
  }
}

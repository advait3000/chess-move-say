import { CUSTOM_EVENT_NAME } from "./const";
import { LichessStreamEvent } from "./cs";

/**
 * This script needs direct access to the page's window object.
 */

interface IWindowLichess {
  lichess: {
    pubsub: {
      emit: (s: string, a: any) => void,
    },
  };
}

/**
 * Attempt to monkey patch the window.lichess.pubsub.emit function.
 * Pass events back to content script.
 */
function hijackPubsub() {
  const w = (window as unknown as IWindowLichess);
  if (!w.lichess ||
    !w.lichess.pubsub ||
    !w.lichess.pubsub.emit) {
    console.log("Could not find lichess object to latch onto.");
    console.log("test", w.lichess);
    return false;
  }

  const original = w.lichess.pubsub.emit;
  w.lichess.pubsub.emit = (name: string, body: any) => {
    const e: LichessStreamEvent = new CustomEvent(CUSTOM_EVENT_NAME, {
      detail: {
        body,
        name,
      },
    });
    document.dispatchEvent(e);

    original.call(w.lichess.pubsub, arguments);
  };
  return true;
}

/**
 * Repeatedly attempt monkey patches until success.
 */
function hijackPubsubTimeout() {
  setTimeout(() => {
    if (!hijackPubsub()) {
      hijackPubsubTimeout();
    }
  }, 100);
}

hijackPubsubTimeout();

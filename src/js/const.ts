/**
 * We need an arbitrary string event name to send from the direct script to the
 * content script.
 */
export const CUSTOM_EVENT_NAME = "chess-shout-event";
